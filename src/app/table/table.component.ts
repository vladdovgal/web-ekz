import { Request, Response } from 'express';
import { ParamsDictionary } from 'express-serve-static-core';
import { model, Document, Schema } from 'mongoose';

export interface Product {
  readonly _id: any;
  readonly name: string;
  readonly model: string;
  readonly price: number;
};
interface ProductResponse {
  readonly products: Product[];
}
interface User extends Document {
  readonly _id: any;
  readonly name: string;
  readonly age: number;
  readonly salary: number;
};

const UserSchema = new Schema({
  name: { type: String, required: true },
  age: { type: Number, required: true },
  salary: { type: Number, required: true }
});
const UserModel = model<User>("UserCollection", UserSchema);


interface IIdParams extends ParamsDictionary {
  readonly id: string;
}

export const deleteUser = (req: Request<IIdParams>, res: Response) =>
  UserModel.findByIdAndDelete(req.params.id)
    .exec()
    .then(
      succ => res.send('deleteUser'),
      err  => res.send(`failed to delete user: ${err.message}`)
    );

//
// export const deleteUser = async(req,res) =>
// {const result = await model.finByIdAndDelete(req.params.id)
//   req.send('deleted')}

const MyProducts = [
  {
    _id: '1',
    name: 'Xiaomi',
    model: 'Model mi 9',
    price: 7777.7
  },
  {
    _id: '2',
    name: 'Apple',
    model: 'iPhone X',
    price: 18999.9
  }];

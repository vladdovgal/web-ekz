import { Component} from '@angular/core';
import {PRODUCTS} from "../product";

@Component({
  selector: 'app-app-products',
  templateUrl: './app-products.component.html',
  styleUrls: ['./app-products.component.css']
})
export class AppProductsComponent {
products = PRODUCTS;
}

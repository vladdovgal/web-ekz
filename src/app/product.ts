export interface Product {
  name: String;
}

export const PRODUCTS = [
  {name: 'Bananas'},
  {name: 'Apples'},
  {name: 'Tomatoes'},
  {name: 'Cucumber'},
  {name: 'Strawberries'},
];

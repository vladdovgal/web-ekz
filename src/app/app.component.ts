import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

// 5.1
// export class AppComponent implements AfterViewInit{
//   @ViewChild('text') myDiv: ElementRef;
//   title = 'my-app';
//   isClicked = false;
//   changeColor() {
//     this.isClicked = !this.isClicked;
//     if (this.isClicked==false) {
//       this.myDiv.nativeElement.style.color = 'red';
//     } else {
//       this.myDiv.nativeElement.style.color = 'green';
//     }
//   }
//
//   ngAfterViewInit(): void {
//   }
//
// }

// 5.2
// export  class AppComponent {
//   title = 'my-app';
//
// status : boolean = false;
//   changeColor(){
//     this.status = !this.status;
//   }
// }


// 4
export class AppComponent {
  title = 'my-app';

}

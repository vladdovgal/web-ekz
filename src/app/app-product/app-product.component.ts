import { Component, Input } from '@angular/core';
import {Product} from "../product";

@Component({
  selector: 'app-app-product',
  templateUrl: './app-product.component.html',
  styleUrls: ['./app-product.component.css']
})
export class AppProductComponent {
  @Input() product: Product;
}
